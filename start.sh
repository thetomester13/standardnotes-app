#!/bin/bash

set -eu

if [[ ! -f /app/data/env ]]; then
    cp /app/pkg/env.production /app/data/env
fi

if [[ ! -f /app/data/.initialized ]]; then
    echo "==> Initializing database and secrets on first run"
    SECRET_KEY_BASE=$(bundle exec rake secret)
    sed -i "s#SECRET_KEY_BASE={SECRET_KEY_BASE}#SECRET_KEY_BASE=$SECRET_KEY_BASE#" /app/data/env
    export SECRET_KEY_BASE=${SECRET_KEY_BASE}

    cd /app/code
    rails db:create db:migrate
    bundle exec rake secret

    touch /app/data/.initialized
fi

echo "==> Update database if necessary"
rails db:migrate

echo "==> Changing permissions"
chmod 777 -R /app/data/log /app/data/tmp
chown -R www-data:www-data /app/data/ /app/data/tmp /app/data/log

echo "==> Staring Standard Notes"

exec /usr/local/bin/gosu cloudron:cloudron bundle exec rails server webrick -e production -b 0.0.0.0

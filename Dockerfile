FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

# Since Standard Notes doesn't have releases, we'll need to 'version' it using commit SHA's
ENV PROJECT_REPO=https://github.com/standardnotes/syncing-server
ENV RELEASE=a508384052621bf4962729bca3e4a5d78cb6b588

RUN mkdir -p /app/code /app/pkg /app/data/db /app/data/tmp /app/data/tmp/cache /app/data/tmp/pids /app/data/tmp/sockets

# StandardNotes Sync Server
RUN mkdir -p /app/code
RUN curl -L "${PROJECT_REPO}/archive/${RELEASE}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code

WORKDIR /app/code

RUN gem install bundler
RUN bundle install

RUN rm -f /app/code/env.sample && ln -sf /app/data/env /app/code/.env

RUN touch /app/code/log/production.log && \
    cp -r /app/code/log /app/data/log && \
    mv /app/code/log /app/code/log.original && \
    ln -s /app/data/log /app/code/log && \
    ln -s /app/data/tmp /app/code/tmp

RUN rm -f /app/code/db/schema.rb && \
    ln -s /app/data/db/schema.rb /app/code/db/schema.rb

COPY start.sh env.production /app/pkg/

CMD [ "/app/pkg/start.sh" ]
